﻿using UnityEngine;
using UnityEngine.Assertions;
using Game;
using GameSystem.Helpers;
using GameSystem.UI;

namespace Units.Components
{
    //------------------------------------------------------------------------------
    // PURPOSE:
    // This script is mothership Controller. Controls movement and use of weapon.
    //------------------------------------------------------------------------------

    
    [AddComponentMenu("IdleNot/Player/Controller")]
    public class Controller : MonoBehaviour
    {
        [SerializeField] private LayerMask _moveLayer;                        // Layer on which ray hit for movement is beaing checked
        [SerializeField] private LayerMask _weaponLayer;                      // Layer on which ray hit for movement is beaing checked
        [SerializeField] private GameArea _gameArea;                     // Reference to game area to check it size
        [SerializeField] private ShootMarker _shootMarker;        // UI marker that shows direction and strength of weapon shoot
        [SerializeField] private GameObject _projectile;                      // Projectile prefab that will be shoot

        private bool _isMoveActive;                                           // When enabled mother ship will do move into targeted position     
        private bool _isShootActive;                                          // When enabled it allow to show shoot direction marker when dragged towards enemy
        private bool _canShoot;           
        
        private void Start()
        {
            Assert.IsNotNull(Camera.main);
            Assert.IsNotNull(_gameArea);
            _shootMarker = GetComponentInChildren<ShootMarker>();
            Assert.IsNotNull(_shootMarker);
            _shootMarker.gameObject.SetActive(false);
        }

        private void Update()
        {
            // If game is on and Spawning is active, show spawn lane and select spawning object
            if (!GameManager.GameIsOn) return;

            // Create hit ray from camera to the scene. 
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            // If mothership move area is hit, move Mothership
            if (Physics.Raycast(ray, out hitInfo, 50, _moveLayer, QueryTriggerInteraction.Ignore))
            {
                // Allow movement if not yet activated
                if (!_isMoveActive) ActivateMovement();
                
                // When mouse button (TO DO: or Touch) is released 
                if (Input.GetMouseButtonUp(0))
                {
                    // move ship if release over move area
                    if (_isMoveActive) MoveToTargetPosition(hitInfo.point.z);
                    // and deactivate weapon if was activated
                    if (_isShootActive) DeactivateWeapon();
                }
                
                // When mouse or (TO DO: Touch) is pressed let show marker to show when needed
                else if (Input.GetMouseButtonDown(0))
                {
                    if (!_isShootActive) ActivateWeapon(); 
                }

                // When mouse with pressed button or (TO DO:) touch is pressed and its point is back again over move area
                else if (Input.GetMouseButton(0))
                {
                    if (_shootMarker.gameObject.activeSelf) DeactivateWeapon();
                    if (!_isShootActive) ActivateWeapon();
                }
            }
            
            // When Mouse or touch is outside move area and above weapon area
            else if (Physics.Raycast(ray, out hitInfo, 50, _weaponLayer, QueryTriggerInteraction.Ignore))
            {
                // When dragged outside move area with button or touch pressed, show weapon direction marker
               if (Input.GetMouseButton(0))
                {
                    if (_isShootActive) ShowShootMarker();
                }
                
                // When button or mouse is released over weapon area, Fire weapon
                if (Input.GetMouseButtonUp(0))
                {
                    if(_canShoot) FireWeapon();
                    DeactivateWeapon();
                }
            }
            
            else
            {
                if (_isMoveActive) DeactivateMovement();
                if (_isShootActive) DeactivateWeapon();
            }
        }

        // Activate and allow ship movement
        private void ActivateMovement()
        { 
            _isMoveActive = true;
        }
        
        // Deactivate allowance of ship movement 
        private void DeactivateMovement()
        {
            _isMoveActive = false;
        }

        // Set target destination
        private void MoveToTargetPosition(float zPos)
        {
            Vector3 pos = transform.localPosition;
            pos.z = zPos;
            float clampedZ = _gameArea.Size.y / 4;                    // Touch area for mothership control is enlarged in height to cover screen height
            pos.z =  Mathf.Clamp(pos.z, -clampedZ, clampedZ);         // Z position is clamped to combat area size
        }
        
        // Weapon marker behaviour
        private void ActivateWeapon()
        {
            _isShootActive = true;
        }
        
        private void ShowShootMarker()
        {
            _shootMarker.gameObject.SetActive(true);
            _isShootActive = false;
            _canShoot = true;
        }

        private void DeactivateWeapon()
        {
            _isShootActive = false;
            _canShoot = false;
            _shootMarker.gameObject.SetActive(false);
        }

        // Use equiped weapon if able to
        private void FireWeapon()
        {
            GameObject newProjectile = Instantiate(_projectile, GameManager.Instance.GameArea.transform);
            newProjectile.transform.position = transform.position;//_shootMarker.LookPoint; 
            newProjectile.GetComponent<Projectile.Projectile>().Initialize(_shootMarker.LookPoint, null);
        }
    }
}