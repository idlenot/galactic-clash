﻿using Game;
using GameSystem.Components;
using GameSystem.CustomVariables.Enums;
using GameSystem.DataSets;
using GameSystem.Helpers;
using GameSystem.UI;
using UnityEngine;
using UnityEngine.Assertions;

namespace Units
{
    //------------------------------------------------------------------------------
    // PURPOSE:
    // This script controls unit spawn. Controls spawn lane z position,
    // reference to spawning unit or skill etc.
    //------------------------------------------------------------------------------

    
    [AddComponentMenu("IdleNot/Units/Unit Spawner")]
    public class UnitSpawner : MonoBehaviour
    {
        #region Singleton
        private static UnitSpawner _instance;
        public static UnitSpawner Instance => _instance;
         
        private void Singleton()
        {
            if (_instance == null) _instance = this;
            else Destroy(gameObject);
        }
        #endregion
        
        [Header("Spawn help objects")]
        [SerializeField] private SpawnLane _spawnLane;
        [SerializeField] private Animator _blueSpawnAreaAnim;
        [SerializeField] private LayerMask _touchLayer;
        [SerializeField] private GameArea _gameArea;
        
        // Place on a game area where player initialize unit spawn
        private Vector3 _hitPoint;        

        private static bool _isActive;
        public static bool IsActive
        {
            get => _isActive;
            set => _isActive = value;
        }
        // Component references
        [Header("Energy Controller Archetype")]
        [SerializeField] private EnergyController _energyController;

        private void Awake()
        {
            Singleton();
        }

        private void Start()
        {
            if (GameManager.Instance.Players.Length == 0) Debug.LogWarning("There are no Player assign");
            Assert.IsNotNull(Camera.main);
            Assert.IsNotNull(_gameArea);
        }

        private void Update()
        {
            // If game is on and Spawning is active, show spawn lane and select spawning object
            if (!GameManager.GameIsOn || !_isActive) return;
            
            // Show marker
                _spawnLane.gameObject.SetActive(true);
                _spawnLane.Activate(true);
                _spawnLane.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            
            // Create hit ray from camera to the scene. 
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            // Render ray in editor during preview
            Debug.DrawRay(ray.origin, ray.direction * 500, Color.cyan);
            
            // If floor is hit set _spawnLane z position to ray hit z position 
            if (Physics.Raycast(ray, out hitInfo, 50, _touchLayer, QueryTriggerInteraction.Ignore))
            {
                // Set position of spawn lane
                _spawnLane.transform.localPosition = new Vector3(0.0f, 0.0f, hitInfo.point.z);
                Vector3 pos = _spawnLane.transform.localPosition;
                pos.z =  Mathf.Clamp(_spawnLane.transform.localPosition.z, -_gameArea.Size.y, _gameArea.Size.y);

                _hitPoint = hitInfo.point;
            }
        }

        // Spawn selected unit in combat zone area
        public void CheckPlayerSpawn(GameObject spawnObject, int cost, Player player)
        {
            // Do not execute Spawn method if following conditions are met
            if (!GameManager.GameIsOn 
                || spawnObject == null 
                || !_energyController.Use(player, cost)) 
                return;
            
            // Spawn selected object
            SpawnObject(spawnObject, player, _hitPoint.x, _spawnLane.transform.localPosition.z);
   
            // Deactivate Spawning
            // TODO: Do it better and proper way 
            // Temporary solution for coping with mother ship shooting when unit was spawned 
            Invoke(nameof(Deactivate), 0.1f);
            _spawnLane.Activate(false);
        }

        // Spawn provided object
        public void SpawnObject(GameObject spawnObject, Player player, float posx, float posz)
        {
            // TODO: Instantiate through Object Pool
            Vector3 newPos = new Vector3(posx, 0, posz);
            GameObject newObject = Instantiate(spawnObject, newPos, Quaternion.identity);
            newObject.transform.parent = GameManager.Instance.GameArea.transform;
            
            // TODO: Initialize Units for both player or AI
            newObject.GetComponent<UnitController>().Initialize(player);
        }

        public Vector2 GetRandomSpawnPosition()
        {
            Vector2 pos;
            pos = _gameArea.Size;

            pos.x = Random.Range(0, pos.x/2);
            pos.y = Random.Range(-pos.y/2, pos.y/2);
            
            return pos;
        }

        private void Deactivate()
        {
            _isActive = false;
        }

        public void ShowSpawnArea()
        {
            Animator anim = _blueSpawnAreaAnim;
            anim.Play("Show");
        }
        
        public void HideSpawnArea()
        {
            Animator anim = _blueSpawnAreaAnim;
            anim.Play("Hide");
        }
    }
}
