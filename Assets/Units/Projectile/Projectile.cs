﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script controls generic projectile behaviour. Auto destroy
// after some distance travelled.
//------------------------------------------------------------------------------

using Game;
using GameSystem.CustomVariables.Enums;
using UnityEngine;

namespace Units.Projectile
{
    [AddComponentMenu("IdleNot/Units/Projectile")]
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private float _speed = 1.0f;
        [SerializeField] private int _power = 25;
        [SerializeField] private int _distToDestroy = 25;

        [SerializeField] private bool _isHoming;
        private Vector3 _targetDestination;
        private bool _isActive;

        private Vector3 _startPos;

        // Check distance of travel, if larger than provided, destroy projectile
        private void Update()
        {
            float distance = Vector3.Distance(_startPos, transform.position);
            if (distance >= _distToDestroy)
            {
                Destroy(gameObject);
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!_isActive) return;
            if (_isHoming) transform.LookAt(_targetDestination);
            transform.position += transform.forward * Time.fixedDeltaTime * _speed;
        }

        public void Initialize(Vector3 lookPoint, Player player)
        {
            _targetDestination = lookPoint;
            transform.LookAt(_targetDestination);
            _startPos = transform.position;
            _isActive = true;
            
            // TODO: Set object layer according to player side 
//            if (player == GameConstants.PLAYER.BLUE) gameObject.layer = LayerMask.NameToLayer("BluePlayer");             // Layer "BluePlayer"
//            if (player == GameConstants.PLAYER.RED) gameObject.layer = LayerMask.NameToLayer("RedPlayer");               // Layer "RedPlayer"
//            if (player == GameConstants.PLAYER.NONE) Destroy(gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            // When projectile hits IDamagable gives damage and destroys self 
            IDamageable target = other.GetComponent<IDamageable>();
            if (target != null)
            {
                target.TakeDamage(_power);
                Destroy(gameObject);
            }
        }
    }
}