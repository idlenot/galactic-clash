﻿// -------------------------------------------
// PURPOSE:
// Interface for taking damage
// -------------------------------------------

using UnityEngine;

namespace Units
{
    public interface IDamageable
    {
        void TakeDamage(int damage);
    }
}
