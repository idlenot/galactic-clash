﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script controls Music and Audio settings in game
//------------------------------------------------------------------------------


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Audio
{
    [AddComponentMenu("IdleNot/Audio/Audio Manager")]
    public class AudioManager : MonoBehaviour
    {
        [SerializeField] private AudioSource[] _music;
        // Start is called before the first frame update
        void Start()
        {
            PlayMusic();
        }

        private void PlayMusic()
        {
            int r = Random.Range(0, _music.Length);
            _music[r].Play();
        }
    }
}