﻿using GameSystem.Components;
using GameSystem.Helpers;
using GameSystem.CustomVariables.Enums;
using GameSystem.UI;
using UnityEngine;
using UnityEngine.SceneManagement;


namespace Game
{
    //------------------------------------------------------------------------------
    // PURPOSE:
    // This script controls Game related events
    //------------------------------------------------------------------------------

    [AddComponentMenu("IdleNot/Game/Game Manager")]
    public class GameManager : MonoBehaviour
    {
        #region Singleton
        private static GameManager _instance;
        public static GameManager Instance => _instance;
         
        private void Singleton()
        {
            if (_instance == null) _instance = this;
            else Destroy(gameObject);
        }
        #endregion
        
        // Game status
        private static bool _gameIsOn;                            // Game is ready to play
        public static bool GameIsOn => _gameIsOn;

        [Header("List of players that take part ini clash")]
        [SerializeField] public Player[] Players;
        
        [Header("List of motherships on combat zone")]
        [SerializeField] public GameObject[] Motherships;
        
        [Header("Area of the game")]
        public GameArea GameArea;                                 // Reference to game area to check it size and spawn objects inside

        // Retrieve a mother ship of provided player from list of available mother ships on scene 
        public Transform GetMothershipObjectOfEnemy(Player player)
        {
            if (Motherships.Length > 0)
                for (int i = 0; i < Motherships.Length; i++)
                    if (Motherships[i].GetComponent<UnitController>().Player != player)
                        return Motherships[i].transform;
            return null;
        }

        private void Awake()
        {
            Singleton();
        }

        private void Start()
        {
            Timer.TimeOutHit += GameIsOver;
            HudPlayerInfo.AllSegmentsDestroyed += GameIsOver;
            _gameIsOn = true;
        }

        private void GameIsOver(Player player)
        {
            Debug.Log("GAME IS OVER");
            _gameIsOn = false;
            // Application.Quit();                                
        }

        private void OnDisable()
        {
            Timer.TimeOutHit -= GameIsOver;
            HudPlayerInfo.AllSegmentsDestroyed -= GameIsOver;
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
