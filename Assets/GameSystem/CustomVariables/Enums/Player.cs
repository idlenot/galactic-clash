﻿using System.Collections.Generic;
using GameSystem.DataSets;
using UnityEngine;

namespace GameSystem.CustomVariables.Enums
{
    // ------------------------------------------------------------------------
    // 
    // Custom Enum Value : Player 
    // Determines player side and its attributes as color, base health etc.
    //
    // ------------------------------------------------------------------------

    [CreateAssetMenu(fileName = "PlayerType", menuName = "Game Data/Enums/Player Type")]
    public class Player : EnumValue
    {
        [Header("Player color")]
        public Color Color;                          // Color for this player
        
        [Header("Player Information")]
        public Sprite Avatar;                        // Player health value [HideInInspector]
        public string Name;                          // Player health value [HideInInspector]

        [Header("Player Play Hand")] 
        public List<Unit> Units;
        
        [Header("Player Mechanic Sets")] 
        public ActionPointsController APC;
        
        // Player Stats
        [HideInInspector] public int CurrentHealth;   // Player health value [HideInInspector]
        //[HideInInspector] public int CurrentAp;     // Player current energy value
        [HideInInspector] public int ApCost;          // Current Skill in use
    }
}
