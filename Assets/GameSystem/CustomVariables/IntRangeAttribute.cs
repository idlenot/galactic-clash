﻿using System;

namespace GameSystem.CustomVariables
{
    // ------------------------------------------------------------------------
    // 
    // Attribute:
    // Show in inspector min and max values of RangedInt as a slider  
    //
    // ------------------------------------------------------------------------
    
    public class IntRangeAttribute : Attribute
    {
        public IntRangeAttribute(int min, int max)
        {
            Min = min;
            Max = max;
        }
        public int Min { get; private set; }
        public int Max { get; private set; }
    }
}