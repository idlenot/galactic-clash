﻿using System;

namespace GameSystem.CustomVariables
{
    // ------------------------------------------------------------------------
    // 
    // Custom Variable: A pair of minimum and maximum values as int 
    //
    // ------------------------------------------------------------------------

    [Serializable]
    public struct RangedInt
    {
        public float Min;
        public float Max;
    }
}