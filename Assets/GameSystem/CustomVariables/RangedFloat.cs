﻿using System;

namespace GameSystem.CustomVariables
{
    // ------------------------------------------------------------------------
    // 
    // Custom Variable: A pair of minimum and maximum values as float 
    //
    // ------------------------------------------------------------------------
        
    [Serializable]
    public struct RangedFloat
    {
        public float Min;
        public float Max;
    }
}