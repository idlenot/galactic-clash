﻿using System;

namespace GameSystem.CustomVariables
{
    // ------------------------------------------------------------------------
    // 
    // Attribute:
    // Show in inspector min and max values of RangedFloat as a slider  
    //
    // ------------------------------------------------------------------------
    
    public class FloatRangeAttribute : Attribute
    {
        public FloatRangeAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }
        public float Min { get; private set; }
        public float Max { get; private set; }
    }
}