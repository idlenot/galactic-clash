﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.AI.Conditions;
using GameSystem.AI.States;

namespace GameSystem.CustomVariables
{
    // ------------------------------------------------------------------------
    // 
    // Custom Variable: A set of States and Decision to make transition
    // between states based on output of provided decision.
    //
    // ------------------------------------------------------------------------

    [System.Serializable]
    public struct Transition
    {
        public Condition Condition;
        public State TrueState;
        public State FalseState;
    }

}