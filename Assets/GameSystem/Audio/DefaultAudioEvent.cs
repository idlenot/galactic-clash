﻿using UnityEngine;
using GameSystem.CustomVariables;

namespace GameSystem.Audio
{
    // ------------------------------------------------------------------------
    // 
    // Audio Event : Simple audio event that plays random clip 
    // from provided array with provided _volume and _pitch settings
    // at target location
    //
    // ------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "Audio/Simple Event")]
    public class DefaultAudioEvent : AudioEvent
    {
        [SerializeField] private AudioClip[] _clips;
        
        [FloatRange(0,1)] [SerializeField] private RangedFloat _volume;
        [FloatRange(0,1)] [SerializeField] private RangedFloat _pitch;
        
        public override void Play(AudioSource source)
        {
            Debug.Log("Play audio: " + source);
            if (_clips.Length == 0) return;

            source.clip = _clips[Random.Range(0, _clips.Length)];
            source.volume = Random.Range(_volume.Min, _volume.Max);
            source.pitch = Random.Range(_pitch.Min, _pitch.Max);
            source.Play();
        }
    }
}