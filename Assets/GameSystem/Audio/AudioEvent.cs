﻿using UnityEngine;

namespace GameSystem.Audio
{
    // ------------------------------------------------------------------------
    // 
    // Blueprint of : AudioEvent
    //
    // ------------------------------------------------------------------------
    
    public abstract class AudioEvent : ScriptableObject
    {
        public abstract void Play(AudioSource source);
    }
}