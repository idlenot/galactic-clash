﻿using System.Collections;
using System.Collections.Generic;
using GameSystem.Helpers;
using UnityEngine;

namespace GameSystem.Components
{
    // ------------------------------------------------------------------------
    // 
    // NavMeshSurface : Sets nav mesh surface to size of the Game Arena 
    // 
    // ------------------------------------------------------------------------

    public class NavMeshSurface : MonoBehaviour
    {
        [SerializeField] private GameArea _gameArena;
        
        private void OnValidate()
        {
            SetSize();
        }

        private void SetSize()
        {
            transform.localScale = new Vector3 (_gameArena.Size.x, _gameArena.Size.y, 1f);
        }
    }
}

