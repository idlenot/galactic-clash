﻿using System.Collections;
using System.Collections.Generic;
using GameSystem.Components;
using GameSystem.Runtime;
using UnityEngine;

namespace GameSystem.Helpers
{   
    [AddComponentMenu("GC/Components/Runtime List Subscriber")]
    public class RuntimeListSubscriber : MonoBehaviour
    {
        [SerializeField] private List<UnitsList> _lists;
        [SerializeField] private UnitController _type;
        
        private void OnEnable()
        {
            for (int i = 0; i < _lists.Count; i++)
            {
                _lists[i].Add(_type);
            }
        }

        private void OnDisable()
        {
            for (int i = 0; i < _lists.Count; i++)
            {
                _lists[i].Remove(_type);
            }
        }
    }
}