﻿using System;
using System.Collections.Generic;

namespace GameSystem.Helpers
{   
    public static class ListExtension 
    {
        public static List<T> Shuffle<T>(this List<T> list) {
            Random rnd = new Random();
            for (var i = 0; i < list.Count; i++)
                list.Swap(i, rnd.Next(i, list.Count));
            return list;
        }
 
        public static void Swap<T>(this IList<T> list, int i, int j) {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }
    }
}