﻿using UnityEngine;

namespace GameSystem.Helpers
{
    //------------------------------------------------------------------------------
    // PURPOSE:
    //
    // This script controls game area and its collider size for showing
    // touchable game zone in editor and to allow touch for spawning units
    // and effects
    //
    //------------------------------------------------------------------------------

    
    [AddComponentMenu("GC/Area/Game Area")]
    public class GameArea : MonoBehaviour
    {
        [SerializeField] private Color _gizmoColor;
        [SerializeField] private Color _gizmoWireColor;
        [SerializeField] private Transform _navMeshSurface;
        
        private Rect _area;
        public Rect Area
        {
            get { return _area; }
        }

        [SerializeField] private Vector2 _size;
        public Vector2 Size
        {
            get { return _area.size; }
            set
            {
                _area = new Rect(value.x * -0.5f, value.y * -0.5f, value.x, value.y);
            }
        }

        private BoxCollider _collider;
//        private MeshCollider _collider;
        
        private void Awake()
        {
            SetSize();
        }
        
        private void OnValidate()
        {
            SetSize();
        }

        private void SetSize()
        {
            Size = _size;
            _collider = GetComponent<BoxCollider>();
            if (_collider != null) _collider.size = new Vector3(_size.x, _size.y, 0.0f);
            if (_navMeshSurface != null) _navMeshSurface.localScale = _collider.size;

        }

        private void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = _gizmoColor;
            Gizmos.DrawCube(Vector2.zero, _area.size);
            Gizmos.color = _gizmoWireColor;
            Gizmos.DrawWireCube(Vector2.zero, _area.size);
        }
    }
}

