﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using GameSystem.Components;

namespace GameSystem.Runtime
{
    // ------------------------------------------------------------------------
    // 
    // RuntimeSet : Units - list of all units in game. 
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "Custom Variables/Runtime List")]
    public class UnitsList : RuntimeList<UnitController>
    {
    }   
}
