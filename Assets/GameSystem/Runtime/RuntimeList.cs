﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem.Runtime
{
    // ------------------------------------------------------------------------
    // 
    // Bluprint of : RuntimeSet - list of any type to store group information. 
    //
    // ------------------------------------------------------------------------

    public abstract class RuntimeList<T> : ScriptableObject
    {
        public List<T> Items = new List<T>();

        public void Add(T t)
        {
            if (!Items.Contains(t)) Items.Add(t); 
        }
        
        public void Remove(T t)
        {
            if (Items.Contains(t)) Items.Remove(t); 
        }
    }
}