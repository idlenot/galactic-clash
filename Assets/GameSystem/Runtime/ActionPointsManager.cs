﻿using System.Collections;
using System.Collections.Generic;
using GameSystem.CustomVariables.Enums;
using UnityEngine;

namespace GameSystem.Runtime
{
    //------------------------------------------------------------------------------
    // 
    // This script calculates 
    // 
    //------------------------------------------------------------------------------

    
    public static class ActionPointsManager
    {
        
        // Is energy fill speed boosted (Last minute of the game)
        private static bool _boostOn;
        
        public static float FillTime (Player player) {
            if (_boostOn) return player.APC.BaseFillUpTime / 2;
            return player.APC.BaseFillUpTime;
        }
        
        // Subtract energy from available pool
        public static bool Use(Player player, int cost)
        {
            if (player.APC.CurrentAp < cost) return false;
            player.APC.CurrentAp -= cost;
            return true;            
        }
        
        // Clamp current fill value between 0 and 1. If is full (1) add energy if not full
        public static float ClampFill(Player player)
        {
            if (player.APC.CurrentAp >= player.APC.TotalAp) return 0;
            if (player.APC.CurrentFill >= 1.0f)
            {
                player.APC.CurrentAp++;
                player.APC.CurrentFill = 0;
            }
            return player.APC.CurrentFill;
        }

        // Clamp current fill value between 0 and provided cost value of unit/skill etc.
        private static float ClampCost(Player player)
        {
            return 0;
        }

        // SpeedUp AP fill time
        public static void BoostFillTime(bool state)
        {
            _boostOn = state;
        }
    }
}
