﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Conditions
{
    // ------------------------------------------------------------------------
    // 
    // Chase condition Template : Stores reference to attributes required to
    // make decision on active state, action.
    //
    // Returns true while target in range is active.
    //
    // ------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "AI/Condition/Target Chased")]
    public class TargetChased : Condition
    {
        public override bool Decide(UnitController controller)
        {
            // When chasing stop this action when target is destroyed.
            bool chaseTargetIsActive = controller.Target.gameObject.activeSelf;
            return chaseTargetIsActive;
        }
    }
}