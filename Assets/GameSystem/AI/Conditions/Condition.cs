﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Conditions
{
    // ------------------------------------------------------------------------
    // 
    // Blueprint of : Condition SO for AI
    //
    // Condition may be positive or negative to the given context
    // Depending on condition state of the unit controller may be changed
    //
    // ------------------------------------------------------------------------
    
    public abstract class Condition : ScriptableObject
    {
        // Returns true or false answer to whatever <StateController> was asked about
        public abstract bool Decide(UnitController controller);
    }
}