﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Conditions
{
    // ------------------------------------------------------------------------
    // 
    // Target Destroyed Decision Template : Stores reference to attributes
    // required to make decision on active state, action.
    //
    // Return true when controller target is destroyed.
    // 
    // ------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "AI/Condition/Target Destroyed")]
    public class TargetDestroyed : Condition
    {
        public override bool Decide(UnitController controller)
        {
            // Check if target has been destroyed
            if (controller.Target == null)
            {
                return true;
            }
            return false;
        }
    }
}