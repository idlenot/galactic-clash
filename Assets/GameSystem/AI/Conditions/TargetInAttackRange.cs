﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Conditions
{
    // ------------------------------------------------------------------------
    // 
    // Detect Target Condition Template : Stores reference to attributes
    // required to make decision on active state, action.
    //
    // Return true when has target in range.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Condition/Target In Attack Range")]
    public class TargetInAttackRange : Condition
    {
        public override bool Decide(UnitController controller)
        {
            return CheckRange(controller);
        }

        // Check if current target is in attack range of this <UnitController>
        private bool CheckRange(UnitController controller)
        {
            if (Vector3.Distance(controller.transform.position, controller.Target.position) <= controller.Unit.AttackRange)
            {
                return true;
            }
            if (controller.Weapon != null && controller.Weapon.activeSelf) controller.Weapon.SetActive(false);
            return false;
        }
    }
}