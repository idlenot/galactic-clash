﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Conditions
{
    // ------------------------------------------------------------------------
    // 
    // Detect Target Condition Template : Stores reference to attributes
    // required to make decision on active state, action.
    //
    // Return true when has target in range.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Condition/Enemy Mothership Detected")]
    public class EnemyMothershipDetected : Condition
    {
        public override bool Decide(UnitController controller)
        {
            return CheckTarget(controller);
        }

        // Check if current target is in attack range of this <UnitController>
        private bool CheckTarget(UnitController controller)
        {
            if (controller.Target != null && 
                controller.Player != controller.Target.GetComponent<UnitController>().Player)
            {
                return true;
            }
            return false;
        }
    }
}