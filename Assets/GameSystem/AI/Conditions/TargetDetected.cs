﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Conditions
{
    // ------------------------------------------------------------------------
    // 
    // Detect Target Decision Template : Stores reference to attributes
    // required to make decision on active state, action.
    //
    // Return true when has target in range.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Condition/Target Detected")]
    public class TargetDetected : Condition
    {
        public override bool Decide(UnitController controller)
        {
            bool targetVisible = TargetInRange(controller);
            return targetVisible;
        }

        // Check if any target is in range of this <UnitController>
        private bool TargetInRange(UnitController controller)
        {
            RaycastHit hit;
            
            if (Physics.SphereCast(controller.transform.position, controller.Unit.DetectRange,
                controller.transform.forward, out hit, controller.Unit.DetectRange))
            {
                if (hit.collider.GetComponent<UnitController>() != null
                    && hit.collider.GetComponent<UnitController>().Player != controller.Player)
                {
                    Debug.Log(controller.gameObject.name + " detects: " + hit.collider.gameObject.name);
                    controller.Target = hit.collider.transform;
                    return true;
                }
            }
            return false;
        }
    }
}
