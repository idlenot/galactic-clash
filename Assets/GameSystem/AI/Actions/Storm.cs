﻿using UnityEngine;
using GameSystem.Components;
using Game;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Storm Action Template : Stores reference to attributes required to
    // act the action
    //
    // Storm to the enemy mothership for final hit.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Actions/Storm")]
    public class Storm : Action
    {
        public override void Act(UnitController controller)
        {
            DoStorm(controller);
        }

        private void DoStorm(UnitController controller)
        {
            // While storming controller object finds enemy mothership and start chase it
            if (controller.Target == null)
            {
                controller.Target = GameManager.Instance.GetMothershipObjectOfEnemy(controller.Player);;
            }
            else
            {
                // While chasing controller object move towards target position until decide not to
                controller.NavMeshAgent.destination = controller.Target.position;
                controller.NavMeshAgent.isStopped = false;
            }
        }
    }
}