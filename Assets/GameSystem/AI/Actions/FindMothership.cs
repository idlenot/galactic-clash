﻿using Game;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Storm Action Template : Stores reference to attributes required to
    // act the action
    //
    // Storm to the enemy mothership for final hit.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Actions/Find Mothership")]
    public class FindMothership : Action
    {
        public override void Act(UnitController controller)
        {
            FindTarget(controller);
        }

        private void FindTarget(UnitController controller)
        {
            // Find enemy mother ship on current game arena
            controller.Target = GameManager.Instance.GetMothershipObjectOfEnemy(controller.Player);
        }
    }
}