﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Storm Action Template : Stores reference to attributes required to
    // act the action
    //
    // Storm to the enemy mothership for final hit.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Actions/Idle Float")]
    public class IdleFloat : Action
    {
        public override void Act(UnitController controller)
        {
            StayIdle(controller);
        }

        private void StayIdle(UnitController controller)
        {
            // TODO: Idle state
            // While storming controller object finds enemy mothership and start chase it
            //controller.Target = null;
        }
    }
}