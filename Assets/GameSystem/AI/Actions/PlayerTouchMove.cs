﻿using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Storm Action Template : Stores reference to attributes required to
    // act the action
    //
    // Storm to the enemy mothership for final hit.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Actions/Player Touch Move")]
    public class PlayerTouchMove : Action
    {
        [Header("Layers on which Touch is checked")]
        [SerializeField] private LayerMask _layer;                        // Layer on which ray hit for movement is being checked
        
        public override void Act(UnitController controller)
        {
            SetDestination(controller);
        }

        private void SetDestination(UnitController controller)
        {
            // Create hit ray from camera to the scene.
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Input.GetMouseButtonUp(0))
            {   
                // If mother ship move area is hit, move mother ship
                if (Physics.Raycast(ray, out hitInfo, 50, _layer, QueryTriggerInteraction.Ignore))
                {
                    Vector3 newDestination = new Vector3(controller.transform.position.x, 0.0f, hitInfo.point.z);
                    //controller.Target.position = hitInfo.point;
                    controller.NavMeshAgent.SetDestination(newDestination);
                }
            }
        }
    }
}