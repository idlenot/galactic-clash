﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Attack Action Template : Stores reference to attributes required to
    // act the action
    //
    // Attack target in StateController object attack range.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Actions/Attack")]
    public class AttackAction : Action
    {
        public override void Act(UnitController controller)
        {
            // Debug.Log(controller.gameObject.name + " is attacking: " + controller.Target.gameObject.name);
            if (controller.Target == null)
            {
                if (controller.Weapon != null && controller.Weapon.activeSelf) controller.Weapon.SetActive(false);
                return;
            }
            Attack(controller);
        }
        
        private void Attack(UnitController controller)
        {
            
            if (Vector3.Distance(controller.transform.position, controller.Target.position) 
                <= controller.Unit.AttackRange 
                && controller.CheckIfCountDownElapsed(controller.Unit.AttackDelay))
            {
                controller.StateTimeElapsed = 0;
                // TODO: Attack() goes here!
                // Give damage to the target
                controller.Target.GetComponent<UnitController>().TakeDamage(controller.Unit.AttackPower);
                
                // Set weapon active
                if (controller.Weapon != null && !controller.Weapon.activeSelf) controller.Weapon.SetActive(true);
            }
        }
    }
}