﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using GameSystem.Components;
using Game;
using GameSystem.DataSets;
using GameSystem.Helpers;
using GameSystem.Runtime;
using GameSystem.UI;
using Units;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Storm Action Template : Stores reference to attributes required to
    // act the action
    //
    // Storm to the enemy mother ship for final hit.
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Actions/Simple Unit Spawner")]
    public class SimpleUnitSpawner : Action
    {
        [Header("Units to spawn")]
        [SerializeField] private Unit[] _units;
        private List<Unit> _availableUnits;
        private UnitSpawner _unitSpawner;
        //[Header("Energy Controller Archetype")]
        //[SerializeField] private EnergyController _energyController;
        //private float _currentFill;

        // Reset data on start
        private void OnEnable()
        {
            _availableUnits.Clear();
        }

        public override void Act(UnitController controller)
        {
            if (_unitSpawner == null) _unitSpawner = FindObjectOfType<UnitSpawner>();
//            if (_energyController == null) Debug.LogError("No Energy controller connected but trying to use");
            SpawnUnit(controller);
        }

        private void SpawnUnit(UnitController controller)
        {
            // Virtual Energy Bar - add energy
//            _currentFill += Time.deltaTime / _energyController.FillUpTime;
//            _currentFill = _energyController.EnergyUp(controller.Player, _currentFill);
            
            controller.Player.APC.CurrentFill += Time.deltaTime / ActionPointsManager.FillTime(controller.Player);
            
            // Shuffle if no more elements in _availableUnits
            if (!_availableUnits.Any()) Shuffle();
            
            // Spawn enemy
            if (ActionPointsManager.ClampFill(controller.Player) <= 0f) Spawn(controller);
        }

        // Spawn new unit
        private void Spawn(UnitController controller)
        {
            if (!ActionPointsManager.Use(controller.Player, _availableUnits[0].Cost)) return;
            
            var pos = _unitSpawner.GetRandomSpawnPosition();
            _unitSpawner.SpawnObject(_availableUnits[0].Prefab, controller.Player, pos.x, pos.y);
            _availableUnits.RemoveAt(0);
        }
        
        // Clear _discardedUnits list and shuffle new _availableUnits list
        private void Shuffle()
        {
            _availableUnits = _units.ToList();
            _availableUnits = ListExtension.Shuffle(_availableUnits);
            
            Debug.Log("------------ SHUFFLE --------------");
            for (int i = 0; i < _availableUnits.Count; i++) Debug.Log(_availableUnits[i].name);
        }
    }
}