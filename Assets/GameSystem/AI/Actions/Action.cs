﻿using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Blueprint of : Action SO for AI
    //
    // ------------------------------------------------------------------------
    
    public abstract class Action : ScriptableObject
    {
        public abstract void Act(UnitController controller);
    }
}