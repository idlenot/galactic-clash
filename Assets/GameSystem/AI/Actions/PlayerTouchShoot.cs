﻿using UnityEngine;
using GameSystem.Components;
using GameSystem.Runtime;
using GameSystem.UI;
using Units;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Touch shoot : Allows player to manually shoot with mother ship  
    // Act as the action
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu(menuName = "AI/Actions/Player Touch Shoot")]
    public class PlayerTouchShoot : Action
    {
        [Header("Layers on which Touch is checked")]
        [SerializeField] private LayerMask _layer;                        // Layer on which ray hit for movement is being checked
        
        public override void Act(UnitController controller)
        {
            Aim(controller);
        }

        private void Aim(UnitController controller)
        {
            if (UnitSpawner.IsActive) return;
            if (controller.GuiElement == null) return;
            // Create hit ray from camera to the scene.
            RaycastHit hitInfo;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            // If mother ship move area is hit, move mother ship
            if (Physics.Raycast(ray, out hitInfo, 50, _layer, QueryTriggerInteraction.Ignore))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    controller.GuiElement.Show();
                }
                
                // Hide UI shoot marker when mouse button or touch is rup
                if (Input.GetMouseButtonUp(0))
                {   
                    if(controller.ActiveSkill != null)
                        if (ActionPointsManager.Use(controller.Player, controller.Player.ApCost))
                            controller.ActiveSkill.Use(controller);
                }
            }
            
            // Hide UI shoot marker when mouse button or touch is rup
            if (Input.GetMouseButtonUp(0))
            {   
                controller.GuiElement.Hide();
            }
        }
    }
}