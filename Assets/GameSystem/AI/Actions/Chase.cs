﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;

namespace GameSystem.AI.Actions
{
    // ------------------------------------------------------------------------
    // 
    // Chase Action Template : Stores reference to attributes required to
    // act the action
    //
    // Chase target if StateController object has in its look range.
    //
    // ------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "AI/Actions/Chase")]
    public class Chase : Action
    {
        public override void Act(UnitController controller)
        {
            if (controller.Target == null) return;
            DoChase(controller);
        }

        private void DoChase(UnitController controller)
        {
            // While chasing controller object move towards target position until decide not to
            controller.NavMeshAgent.destination = controller.Target.position;
            controller.NavMeshAgent.isStopped = false;
        }
    }
}