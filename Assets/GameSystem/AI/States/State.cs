﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using GameSystem.AI.Actions;
using GameSystem.Components;
using GameSystem.CustomVariables;

namespace GameSystem.AI.States
{
    // ============================================================================
    // Purpose
    // ============================================================================
    //
    // This is a default <State> SO asset as a template for custom States.
    // It is a set of Actions that represents custom AI Behaviour.
    //
    // ----------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "AI/State")]
    public class State : ScriptableObject
    {
        // An array of actions available for this state
        // Actions determine behaviour of this Unit
        // Actions are checked and executed if true decision is made during this state
        public Action[] Actions;

        // An array of transitions available for this state
        // Array of conditions to meet in given context (ai behaviour)
        // Depends on condition if its true or false another state is selected 
        public Transition[] Transitions;

        // Default in editor gizmo color for test purpose
        public Color SceneGizmoColor = Color.gray;

        public void UpdateState (UnitController controller)
        {
            // This Update is called by the controller object
            DoActions(controller);
            CheckTransitions(controller);
        }

        // Act actions it holds in this behaviour set array
        private void DoActions(UnitController controller)
        {
            for (int i = 0; i < Actions.Length; i++)
            {
                Actions[i].Act(controller);
            }
        }

        private void CheckTransitions(UnitController controller)
        {
            for (int i = 0; i < Transitions.Length; i++)
            {
                // Check if decision is positive or negative (true / false)?
                bool conditionMet = Transitions[i].Condition.Decide(controller);
                // Send true : false state of the decision to set object into another state
                controller.TransitionToState(conditionMet ? Transitions[i].TrueState : Transitions[i].FalseState);
            }
        }
    }
}