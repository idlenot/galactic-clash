﻿using GameSystem.Components;

namespace GameSystem.Interfaces
{
    public interface IHealthBar
    {
        void AddValue(UnitController controller, int value);
    }
}
