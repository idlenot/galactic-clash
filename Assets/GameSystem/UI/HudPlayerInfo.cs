﻿using System.Collections.Generic;
using UnityEngine;
using GameSystem.CustomVariables.Enums;
using UnityEngine.UI;
using GameSystem.Components;

namespace GameSystem.UI
{
        
    public class HudPlayerInfo : MonoBehaviour
    {
        // Start is called before the first frame update
        public delegate void HealthBarStatus(Player player);                    // Delegate to inform about segment destruction
        public static event HealthBarStatus SegmentDestroyed;
        public static event HealthBarStatus AllSegmentsDestroyed;    
        
        [Header("Health Elements")]
        [SerializeField] private Segment[] _segment;                            // Array of connected life segments
        private List<int> _segmentHealth = new List<int>();                     // List of segments health values
        private int _currentSegmentHealth;                                      // Current segment health value
        private int _currentHealth;                                             // Current segment health value
        [SerializeField] private Text _healthText;                              // Text field that shows current health
        
        [Header("Player it belongs to")]
        public Player Player;
        [SerializeField] private Text _playerName;                              // Text field that shows player name
        [SerializeField] private Image _avatarImage;
        

        // Send information to all health bar segments what color they are.
        // Visualize in UI player health status (bars and text information).
        // Initialized by player <Mothership> script that contains player stats information.
        public void Initialize(Player player)
        {
            Player = player;
            // Set player name
            if (player.Name != "") _playerName.text = player.Name;
            else _playerName.text = "Player " + player.name;
            // Set player avatar image
            if (player.Avatar != null) _avatarImage.sprite = player.Avatar;
            
            // Divide health by segments count and store values in it
            int h = player.CurrentHealth / _segment.Length;
            // Save rest from dividing to be added into last segment
            int r = player.CurrentHealth - h * _segment.Length;

            _currentHealth = player.CurrentHealth;
            _healthText.text = _currentHealth.ToString(); 
            
            // Clear and save values into list 
            _segmentHealth.Clear();
            for (int i = 0; i < _segment.Length; i++)
            {
                // Save first segment value as _currentAmount of health (_segment[0] comes first to subtract from)
                if (i == 0) _currentSegmentHealth = h;
                // Last segment includes small rest part from division  
                if (i == _segment.Length - 1) h += r;
                // Save Health value of segments in the health list
                _segmentHealth.Add(h);
                _segment[i].Initialize(Player.Color);
            }
            
            _segment[0].Activate();
        }
        
        // Add value to current health
        // Update Health information in UI. Subtract damage from active health bar segment. Damage that was over current Active Health bar's segment value
        // is not affecting another segment. The damage taken is buffered; 
        public void UpdateHealth(UnitController controller, int value)
        {
            // Check if value is negative and segment health won't drop below 0. If do activate next segment
            if (value < 0 && _currentSegmentHealth <= Mathf.Abs(value))
                value = -_currentSegmentHealth;//_currentSegmentHealth = 0;
            
            // Set Current Health
            _currentHealth += value;  
            _currentSegmentHealth += value;
            
            // Check segments and do actions on active one
            for (int i = 0; i < _segment.Length; i++)
            {
                if (_segment[i].IsActive)
                {
                    float size = (float)_currentSegmentHealth / _segmentHealth[i];
                    _segment[i].SetSize(size); 
                    
                    // Activate next or previous segment
                    if (_currentSegmentHealth == 0) ActivateNextSegment(_segment[i]);
                }
            }
            
            // Update player HUD health text field
            _healthText.text = _currentHealth.ToString(); 
        }

        // When Active health bar segment was depleted or fully charged activate next one
        private void ActivateNextSegment(Segment segment)
        {
            // Send signal that segment has been destroyed
            if (SegmentDestroyed != null) SegmentDestroyed(Player);
            
            // Find index of currently active segment 
            int segmentIndex = System.Array.IndexOf(_segment, segment);
            
            // Choose next segment
            segmentIndex ++;
            
            // If all segments are destroyed send signal to end game
            if (segmentIndex >= _segment.Length)
            {
                if (AllSegmentsDestroyed != null)
                    AllSegmentsDestroyed(Player);
                return;
            }
            
            // activate next segment
            if (_segment[segmentIndex] != null)
            {
                _currentSegmentHealth = _segmentHealth[segmentIndex];
                _segment[segmentIndex].Activate();
            }
        }

        // Change values when attributes in editor has changed
        private void OnValidate()
        {
            Initialize(Player);
        }
    }
}