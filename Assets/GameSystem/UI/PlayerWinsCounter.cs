﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script counts player wins and update it on GUI (stars next to timer
// Event delegate is sent from <UI.Player.HealthBar> 
//------------------------------------------------------------------------------

using Game;
using GameSystem.CustomVariables.Enums;
using GameSystem.UI;
using UnityEngine;
using UnityEngine.UI;

namespace GameSystem.UI
{
    [AddComponentMenu("IdleNot/UI/Player Win Counter")]
    public class PlayerWinsCounter : MonoBehaviour
    {
        [SerializeField] private Text _blueText;
        [SerializeField] private Text _redText;

        private int _blueWins;
        private int _redWins;

        private void Start()
        {
            HudPlayerInfo.SegmentDestroyed += ScoreWin;
            _blueText.text = _blueWins.ToString();
            _redText.text = _redWins.ToString();
        }

        private void OnDisable()
        {
            HudPlayerInfo.SegmentDestroyed -= ScoreWin;
        }

        private void ScoreWin(Player player)
        {
            print("Scores: " + player);
            
            // TODO: make check without string
            if (player.name == "Red")
            {
                _blueWins++;
                _blueText.text = _blueWins.ToString();
            }
            else if (player.name == "Blue")
            {
                _redWins++;
                _redText.text = _redWins.ToString();
            }
        }
    }
}