﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GameSystem.DataSets.Skills;
using GameSystem.Components;

namespace GameSystem.UI
{
    //------------------------------------------------------------------------------
    //
    // This script is mother ship Controller. Controls movement and use of weapon.
    // 
    //------------------------------------------------------------------------------

    
    [AddComponentMenu("IdleNot/UI/Skill Use Button")]
    public class SkillUseButton : GuiElement, IPointerDownHandler
    {
        [Header("Set color to this Image")]
        [SerializeField] private Image _button;
        
        [Header("Image of the skill icon")]
        [SerializeField] private Image _icon;
        
        private void Awake()
        {
            Canvas = GetComponent<CanvasGroup>();
        }

        // Set up button button
        public override void Initialize(UnitController controller)
        {
            base.Initialize(controller);
            SetColor(controller.Player.Color);
            Hide();
        }
        
        // Update is called once per frame
        private void Update()
        {
            // Set position of this gui to unit position it belongs to 
            if (Controller != null) transform.position = Controller.transform.position + Vector3.up;
            else Discard();        // Destroy this health bar if unit it belongs to is destroyed
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            Debug.Log(Controller.gameObject.name + " skill clicked.");
            // Send signal to <OnUseSkill> to use the skill
            if(Controller.ActiveSkill != null) Controller.ActiveSkill.Use(Controller);
            Hide();
        }

        public override void Activate(UnitController controller)
        {
            Show();
        }


        // Set color of the boarder line to player specific
        private void SetColor(Color color)
        {
            _button.color = color;
        }

        // Show icon of the skill in this button
        public void SetIcon(Image icon)
        {
            _icon = icon;
        }
    }
}
