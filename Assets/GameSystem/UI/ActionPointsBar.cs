﻿using System.Linq;
using Game;
using GameSystem.CustomVariables.Enums;
using GameSystem.DataSets;
using GameSystem.Runtime;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace GameSystem.UI
{
    //------------------------------------------------------------------------------
    // PURPOSE:
    // This script controls energy bar of the player
    //------------------------------------------------------------------------------
    
    [AddComponentMenu("IdleNot/UI/Energy Bar")]
    public class ActionPointsBar : MonoBehaviour
    {
        #region Singleton
        private static ActionPointsBar _instance;
        public static ActionPointsBar Instance => _instance;
         
        private void Singleton()
        {
            if (_instance == null) _instance = this;
            else Destroy(gameObject);
        }
        #endregion
        
        [Header("what player it's connected to")]
        public Player Player;
        
        // Array of connected life segments
        [Header("Connected segment components")]
        [SerializeField] private Segment[] _segment;
        
        [Header("Text object to show current energy")]
        [SerializeField] private Text _text;                                    // Text on UI that shows current amount o energy available

        private void Awake()
        {
            Singleton();
        }

        // Start is called before the first frame update
        void Start()
        {
            Assert.IsNotNull(_text);
            Initialize();
        }

        private void Initialize()
        {
            for (int i = 0; i < _segment.Count(); i++)
            {
                _segment[i].SetColorState();
                if (i < Player.APC.CurrentAp) _segment[i].Activate();
                else _segment[i].SetSize(0);
            }
        }
        
        // Update energy text on GUI
        void Update()
        {
            // Show text
            _text.text = Player.APC.CurrentAp.ToString();
        }

        // Set bar size
        private void FixedUpdate()
        {
            if (!GameManager.GameIsOn) return;
            EnergizeBar();
        }

        private void EnergizeBar()
        {
            // check if current energy bar is filled up, if it is set another one
//            _currentFill += Time.deltaTime / _energyController.FillUpTime;
//            _currentFill = _energyController.EnergyUp(Player, _currentFill);
            
            Player.APC.CurrentFill += Time.deltaTime / ActionPointsManager.FillTime(Player);
            
            if (ActionPointsManager.ClampFill(Player) <= 0f)
            {
                _segment[Player.APC.CurrentAp].Activate();
            }
            // Set active segment fill size
            _segment[Player.APC.CurrentAp].SetSize(ActionPointsManager.ClampFill(Player));
        }
    }
}