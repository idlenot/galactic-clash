﻿using System.Linq;
using Game;
using GameSystem.CustomVariables.Enums;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

namespace GameSystem.UI
{
    //------------------------------------------------------------------------------
    // PURPOSE:
    // This script controls energy bar of the player
    //------------------------------------------------------------------------------

    
    [AddComponentMenu("IdleNot/UI/Energy Bar")]
    public class EnergyBar1 : MonoBehaviour
    {
//        #region Singleton
//        private static EnergyBar _instance;
//        public static EnergyBar Instance => _instance;
//         
//        private void Singleton()
//        {
//            if (_instance == null) _instance = this;
//            else Destroy(gameObject);
//        }
//        #endregion

        
        [Header("what player it's connected to")]
        public Player Player;                                
        [Header("Connected segment components")]
        [SerializeField] private Segment[] _segment;                            // Array of connected life segments
        //[SerializeField] private float _speed = 3.0f;                         // How fast energy segment is recharging
        [Header("Text object to show current energy")]
        [SerializeField] private Text _text;                                    // Text on UI that shows current amount o energy available
        private int _totalEnergy = 10;                                          // Maximum amount of energy
        [Range(0, 10)] [SerializeField] private int _startingEnergy = 5;        // How much energy has player on start
        private int _currentEnergy;                                      // How much energy is currently available
        public int CurrentEnergy => _currentEnergy;

        [Header("Seconds to fill up one segment")]
        [SerializeField] private float _fillUpTime = 3.0f;                      // How long in seconds energy segment needs to fill up
        public float FillUpTime {
            get
            {
                if (_energyBoostActive) return _fillUpTime / 2;
                return _fillUpTime;
            } 
        }

        private bool _energyBoostActive;
        private float _currentSize;
        public float CurrentSize => _currentSize;

//        private void Awake()
//        {
//            Singleton();
//        }

        // Start is called before the first frame update
        void Start()
        {
            Assert.IsNotNull(_text);
            Timer.LastMinuteHit += BoostEnergy;
            Initialize();
        }

        private void Initialize()
        {
            _currentEnergy = _startingEnergy;
            for (int i = 0; i < _segment.Count(); i++)
            {
                _segment[i].SetColorState();
                if (i < _startingEnergy) _segment[i].Activate();
                else _segment[i].SetSize(0);
            }
        }
        
        // Update energy text on GUI
        void Update()
        {
            // Show text
            _text.text = _currentEnergy.ToString();
            // Update Player info
            //Player.CurrentAp = _currentEnergy;
        }

        // Set bar size
        private void FixedUpdate()
        {
            if (!GameManager.GameIsOn) return;
            EnergizeBar();
        }

        private void EnergizeBar()
        {
            if (_currentEnergy >= _totalEnergy) return;
            
            _currentSize += Time.fixedDeltaTime / FillUpTime;
            _segment[_currentEnergy].SetSize(_currentSize);
            if (_currentSize >= 1.0f)
            {
                _segment[_currentEnergy].Activate();
                _currentSize = 0.0f;
                _currentEnergy++;
            }
        }
        
        public bool HasEnergy(int cost, Player player)
        {
            if (_currentEnergy >= cost)
            {
                SubtractEnergy(cost, player);
                return true;
            }
            return false;
        }

        private void SubtractEnergy(int amount, Player player)
        {
            if (_currentEnergy == _totalEnergy)
            {
                _currentEnergy --;
            }

            for (int i = 0; i <= amount; i++)
            {
                _segment[_currentEnergy].SetSize(0);
                if (_currentEnergy > 0) _currentEnergy --;
            }
        }

        private void BoostEnergy(Player player)
        {
            _energyBoostActive = true;
        }

        private void OnDisable()
        {
            Timer.LastMinuteHit -= BoostEnergy;
        }
    }
}