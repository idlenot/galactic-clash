﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace GameSystem.UI
{
    
    //------------------------------------------------------------------------------
    // 
    // This script controls animation of hud element during game play. 
    //  
    //------------------------------------------------------------------------------

    [RequireComponent(typeof(Animator))]
    public class HudAnimation : MonoBehaviour
    {
        private Animator _anim;
        [Header("Animation Controls")]
        [SerializeField] private bool _showOnStart;
//        [SerializeField] private AnimationClip _showAnim;
//        [SerializeField] private AnimationClip _hideAnim;
        
        [SerializeField] private AnimationCurve _curve;
        [SerializeField] private WrapMode _mode;

        [SerializeField] private float _speed;                // Distance and direction of movement
        [SerializeField] private Vector3 _moveDistance;       // Distance and direction of movement
        private Vector3 _originPos;                           // Start move position
        private Vector3 _targetPos;                           // End move position

        private RectTransform _rt;                            // Reference to rect transform of this object
        private bool _isActive;
        
        private void Awake()
        {
            _anim = GetComponent<Animator>();
            _rt = GetComponent<RectTransform>();
        }

        // Start is called before the first frame update
        private void Start()
        {
            // Play Show Animation
            _originPos = _rt.localPosition;
            if (_showOnStart) Show();
        }

        public void Hide()
        {
            _rt.localPosition = _originPos;
            _targetPos = _originPos + _moveDistance;
            _isActive = true;
        }
        
        public void Show()
        {
            _rt.localPosition = _originPos + _moveDistance;
            _targetPos = _originPos;
            _isActive = true;
        }

        private void Update()
        {
            if (!_isActive) return;
            // The step size is equal to speed times frame time.
            float step = _speed * Time.deltaTime;

            // Move our position a step closer to the target.
            _rt.localPosition = Vector3.MoveTowards(_rt.localPosition, _targetPos, step);
            if (_rt.localPosition == _targetPos) _isActive = false;
        }
    }
}