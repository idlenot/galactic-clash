﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script selects card and sends to <UnitSpawner> that object will
// be spawned next.
//------------------------------------------------------------------------------

using Game;
using Units;
using UnityEngine;
using UnityEngine.EventSystems;

namespace GameSystem.UI
{
    [AddComponentMenu("IdleNot/UI/Cards/Card Selector")]
    public class CardSelector : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler
    {

        private Card _card;

        private void Start()
        {
            _card = GetComponent<Card>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            SelectCard(true);
        }
        
        public void OnPointerUp(PointerEventData eventData)
        {
            if (UnitSpawner.IsActive)
            {
                UnitSpawner.Instance.CheckPlayerSpawn(_card.SpawnObject, 2, GameManager.Instance.Players[0]);
                UnitSpawner.Instance.HideSpawnArea();
            }
            SelectCard(false);
        }
        
        public void OnBeginDrag(PointerEventData eventData)
        {
            // TODO: Check if selected card is loaded and ready to use
            UnitSpawner.IsActive = true;
            UnitSpawner.Instance.ShowSpawnArea();
        }

        public void OnDrag(PointerEventData eventData)
        {
            
        }
        
        public void OnEndDrag(PointerEventData eventData)
        {
            
        }

        public void SelectCard(bool hasObject)
        {
            if (hasObject)
            {
                // What happen when a card has been selected and
//                print("Card selected: " + name);
            }

            if (!hasObject)
            {
                // What happen when a card has been unselected
//                print("Card NOT selected!");
            }
        }
    }
}