﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script counts gameplay time down. When reaches 0:00 send game over
// signal.
//------------------------------------------------------------------------------

using System;
using Game;
using GameSystem.CustomVariables.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace GameSystem.UI
{
    [AddComponentMenu("IdleNot/UI/Timer")]
    public class Timer : MonoBehaviour
    {
        public delegate void TimeCounter(Player player);                // Delegate to inform about timer status
        public static event TimeCounter LastMinuteHit;
        public static event TimeCounter TimeOutHit;
    
        [SerializeField] private float _startTime;                                     // How log timer will count in seconds
        [SerializeField] private Text _text;                                           // Text that displays timer value 

        private float _currentTime;                                                    // Keeps track of current game play time 
        private bool _isCounting;                                                      // Is timer actually counting time down

        private bool _lastMinuteHit;                                                   // Controller that sets true when last minute on a timer has hit
    
        // Start is called before the first frame update
        private void Start()
        {
            TimeOutHit += TimeOut;
            StartCount();
        }

        // Update is called once per frame
        private void FixedUpdate()
        {
            if (!_isCounting || !GameManager.GameIsOn) return; 
            Count();
        }

        private void StartCount()
        {
            _currentTime = _startTime;
            _isCounting = true;
        }

        private void Count()
        {
            _currentTime -= Time.fixedDeltaTime;
            // When reaches 60s send signal that s the last minute of gameplay
            if (_currentTime <= 60 && !_lastMinuteHit && LastMinuteHit != null)
            {
                LastMinuteHit(null);
                _lastMinuteHit = true;
            }
            // When reaches 0s Game is Over
            if (_currentTime <= 0 && TimeOutHit != null) TimeOutHit(null);
            TimeSpan span = TimeSpan.FromSeconds((int)_currentTime);
            _text.text = span.Minutes + ":"+ span.Seconds;
        }
    
        private void TimeOut(Player player)
        {
            _isCounting = false;
        }
    }
}