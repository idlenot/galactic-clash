﻿using System.Collections;
using GameSystem.UI;
using UnityEngine;
using GameSystem.CustomVariables.Enums;

namespace GameSystem.UI
{
    //------------------------------------------------------------------------------
    // 
    // This script controls behaviour of hud elements during game play.
    // - show when energy is boost ( 60s to end game) etc.
    // - hide HUD and show end screen
    //  
    //------------------------------------------------------------------------------

    
    [AddComponentMenu("IdleNot/UI/Info Module")]
    public class HudController : MonoBehaviour
    {
        [Header("HUD Animation Controllers")]
        [SerializeField] private GameObject _topPanel;
        private Animator _topPanelAnim;
        [SerializeField] private GameObject _bottomPanel;
        private Animator _bottomPanelAnim;
        [SerializeField] private GameObject _quickInfo;
        private Animator _quickInfoAnim;
        [SerializeField] private GameObject _endGame;
        private Animator _endGameAnim;

        
        // Start is called before the first frame update
        void Start()
        {
            Timer.LastMinuteHit += ShowEnergyBoost;
            Timer.TimeOutHit += ShowEndGame;
            HudPlayerInfo.AllSegmentsDestroyed += ShowEndGame;
            
            // Set modules to inactive
            _topPanelAnim = _topPanel.GetComponent<Animator>();
            _bottomPanelAnim = _bottomPanel.GetComponent<Animator>();
            
            _quickInfoAnim = _quickInfo.GetComponent<Animator>();
            _quickInfo.SetActive(false);
            
            _endGameAnim = _endGame.GetComponent<Animator>();
            _endGame.SetActive(false);
        }

        // Show Energy boost info
        private void ShowEnergyBoost(Player player)
        {
            _quickInfo.SetActive(true);
            _quickInfoAnim.Play("Show");
            Invoke(nameof(HideEnergyBoost), 3.0f);
        }
        
        private void HideEnergyBoost()
        {
            _quickInfoAnim.Play("Hide");
            StartCoroutine(DeactivateAnimatorObject(_quickInfo));
        }

        // Deactivate HUD element when invoked to HIDE
        IEnumerator DeactivateAnimatorObject(GameObject animatorObject)
        {
            yield return new WaitForSeconds(1f);
            animatorObject.SetActive(false);
        }

        // Show End Game info
        private void ShowEndGame(Player player)
        {
            // Hide HUD
            _topPanelAnim.Play("Hide");
            _bottomPanelAnim.Play("Hide");
            
            // Activate End Screen
            _endGame.SetActive(true);
            _endGameAnim.Play("Show");
        }

        private void HideEndGame()
        {
            _endGameAnim.Play("Hide");
            StartCoroutine(DeactivateAnimatorObject(_endGame));
        }
    }
}