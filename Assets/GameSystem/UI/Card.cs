﻿using Game;
using GameSystem.CustomVariables.Enums;
using UnityEngine;
using UnityEngine.UI;
using GameSystem.DataSets;
using GameSystem.Runtime;

namespace GameSystem.UI
{
    //------------------------------------------------------------------------------
    // 
    // This script controls card in holder behavior. Charge energy and
    // Activate to use when fully charged.
    // 
    //------------------------------------------------------------------------------

    
    [AddComponentMenu("IdleNot/UI/Cards/Card")]
    public class Card : Segment
    {
        [Header("Card specifics")]
        [SerializeField] private Unit _unit;
        [SerializeField] private Image[] _iconImage;                                // Reference to Background Image of the Segment gameObject
        [SerializeField] private Text _costText;                               // Text field that show cost fo the card in UI
        public GameObject SpawnObject => _unit.Prefab;

        [SerializeField] private Image _loader;
        // Keep tracks of current card image fill amount
        [Range(0.0f, 1.0f)] [SerializeField] private float _cardFill;       

        private Player _player;
        
        // Reference to components
        private Animator _anim;

        private void Start()
        {
            // Get access to <animator> of the card
            _anim = GetComponent<Animator>();
            // Get reference to Player One
            _player = GameManager.Instance.Players[0];
        }

        // Set initial attributes values to the card object
        private void Initialize()
        {
            // Change image icon on the card
            foreach (var icon in _iconImage)
            {
                icon.sprite = _unit.Icon;
            }
            
            // Set cost value that show on the card
            _costText.text = _unit.Cost.ToString();
        }

        private void OnValidate()
        {
            if(_unit != null) Initialize();
        }

        private void Update()
        {
            if (!GameManager.GameIsOn) return;
            
            //_cardFill += Time.deltaTime / ActionPointManager.FillTime(_player);
            
            // If energy level is lower than card cost deactivate card and show current energy load status
            //if (EnergyBar.Instance.CurrentEnergy < _unit.Cost)
            if (_player.APC.CurrentAp < _unit.Cost)
            {
                if (IsActive) IsActive = false;
                _anim.Play("Hide");
                SetSize(CalculateSize());
                
            }
            else
            {
                if (IsActive) return;
                IsActive = true;
                _anim.Play("Show");
                SetColorState();
            }
        }

        // Set image fill amount appropriate to current energy level 
        private new void SetSize(float value)
        {
            if (value <= 0f)
            {
                value = 0f;
            }

            if (value >= 1f)
            {
                value = 1f;
            }
            SetColorState();
            _loader.fillAmount = value;
        }

        public override void SetColorState()
        {
            // Set default alpha to basic segment elements
            base.SetColorState();
            
            // and text
            Color newTextColor = _costText.color;
            newTextColor.a = IsActive ? ActiveAlpha : NotActiveAlpha;
            _costText.color = newTextColor;
        }

        private float CalculateSize()
        {
            _cardFill = (GameManager.Instance.Players[0].APC.CurrentAp 
                            + GameManager.Instance.Players[0].APC.CurrentFill) 
                            / _unit.Cost + 0.01f;
            return _cardFill;
        }
    }
}