﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script is mothership Controller. Controls movement and use of weapon.
//------------------------------------------------------------------------------

using Units.Components;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UI.Joystick
{
    [AddComponentMenu("IdleNot/UI/Joystick")]
    public class Joystick : MonoBehaviour, IPointerClickHandler, IDragHandler
    {
        private bool _hasMoved;

        private void Start()
        {
            
        }

        public void OnPointerClick(PointerEventData pointerEventData)
        {
            //Output to console the clicked GameObject's name and the following message. You can replace this with your own actions for when clicking the GameObject.
            Debug.Log(name + " Game Object Clicked!");
        }

        void IDragHandler.OnDrag(PointerEventData eventData)
        {
            DrawLine(eventData);
        }

        private void DrawLine(PointerEventData eventData)
        {
            Vector3 touchPosition = Camera.main.ScreenToWorldPoint(eventData.position);
        
            Debug.DrawLine(transform.parent.position, touchPosition, Color.red);
            print(transform.name + " " + touchPosition.y);
            Vector3 joyPosition = transform.localPosition;
            joyPosition.z = touchPosition.z + 31;
            transform.localPosition = joyPosition;
        }
    }
}
