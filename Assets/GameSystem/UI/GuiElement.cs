﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using GameSystem.DataSets.Skills;
using GameSystem.Components;

namespace GameSystem.UI
{
    //------------------------------------------------------------------------------
    // 
    // Base GUI Element Class
    // Object on screen that are somehow interactive on the screen 
    // 
    //------------------------------------------------------------------------------

    public abstract class GuiElement : MonoBehaviour
    {
        protected CanvasGroup Canvas;
        protected UnitController Controller;

        public abstract void Activate(UnitController controller);
        
        public virtual void Initialize(UnitController controller)
        {
            Controller = controller;
            Canvas = GetComponent<CanvasGroup>();
        }

        // Show button when skill is ready
        public virtual void Show()
        {
            Canvas.alpha = 1f;
        }

        // Check if skill button has been shown 
        public bool IsSeen()
        {
            return Canvas.alpha > 0f;
        }

        // Hide button after skill was used
        public virtual void Hide()
        {
            Canvas.alpha = 0f;
        }
        
        public virtual void Discard()
        {
            Destroy(gameObject);
        }
    }
}
