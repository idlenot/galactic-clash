﻿using GameSystem.Components;
using GameSystem.CustomVariables.Enums;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UI.Image;
using Plane = UnityEngine.Plane;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace GameSystem.UI
{
    //------------------------------------------------------------------------------
    // 
    // This script controls rotation and length of player controller shoot marker.
    // 
    //------------------------------------------------------------------------------

    [AddComponentMenu("IdleNot/UI/Shoot Marker")]
    public class ShootMarker : GuiElement
    {
        [Header("Image Rect Transform that width will be adjusted")]
        [SerializeField] private RectTransform _backgroundImage;
        [SerializeField] private RectTransform _coverImage;
        
        [Header("Image Rect Transform that width will be adjusted")]
        [SerializeField] private GameObject _energyTagObject;          // Energy Tag child game object of this prefab
        [SerializeField] private Image _energyTagImage;                // Reference to Energy tag image
        [SerializeField] private Text _energyTagText;                  // Reference to Energy tag text
        
        // shoot marker rotation values
        private Vector3 _lookPoint = Vector3.zero;
        public Vector3 LookPoint => _lookPoint;
        
        
        // Base values to calculate new ratio based on actual screen size
        private float _baseScreenWidth = 1920;
        private float _baseMaxWidth = 550;
        private float _baseRatio = 165;
        
        public override void Initialize(UnitController controller)
        {
            base.Initialize(controller);
            Hide();
        }
        
        // Activate this marker to use and show it
        public override void Activate(UnitController controller)
        {
            if (!IsSeen()) Show();
        }

        // Update is called once per frame
        void Update()
        {
            // Destroy this gui element if unit it belongs to is destroyed
            if (Controller == null) Discard();
            if (!IsSeen()) return;
            // Set position of this gui to unit position it belongs to 
            transform.position = Controller.transform.position;
          
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Plane checkPlane = new Plane(Vector3.up, Vector3.zero);
            float rayLenght;

            if (checkPlane.Raycast(ray, out rayLenght))
            {
                // Show marker line with length of distance from source to touch
                // Check if can consume that amount of energy
                Vector2 touchPoint = Camera.main.WorldToScreenPoint(ray.origin);
                SetSize(touchPoint.x/CalculateRatio());
                
                // Rotation
                // Rotate(ray, rayLenght);
            }
        }

        // Calculate ratio of the width divider
        private float CalculateRatio()
        {
            // Calculate and return
            return _baseRatio * (Screen.width / _baseScreenWidth);
        }

        // Set the size of the shoot marker
        private void SetSize(float touchX)
        {
            // Set minimum and maximum width
            float maxWidth = _baseMaxWidth / CalculateRatio(); 
            float minWidth = maxWidth / 3;
            
            // Make sure that touch X point is not less than _minWidth
            if (touchX > maxWidth) touchX = maxWidth;
            if (touchX < minWidth) touchX = minWidth;
            
            // Set width of background - required energy bar 
            Vector2 touchWidth = new Vector2(touchX, 1.0f);
            _backgroundImage.sizeDelta = touchWidth;
            
            // Set width of cover - current energy bar
            Vector2 energyWidth = new Vector2(CalculateWidthOnEnergyBarSize(minWidth, touchX), 1.0f);
            _coverImage.sizeDelta = energyWidth;
            
            // Set position of energy tags
            SetEnergyTag(minWidth);
            ShowEnergyConsumption(touchX, minWidth);
        }

        // Relocate energy tags in proper position according to the screen size
        private void SetEnergyTag(float minWidth)
        {
            // Set position of EnergyTag
            Vector3 newPos = Vector3.zero;
            newPos.x = 1 + minWidth;
            _energyTagObject.transform.localPosition = newPos;
        }

        // Show how many energy is needed to shoot
        private void ShowEnergyConsumption(float touchX, float minWidth)
        {
            if (touchX >= minWidth && touchX < 2 * minWidth) Controller.Player.ApCost = 1;
            if (touchX >= 2 * minWidth && touchX < 3 * minWidth) Controller.Player.ApCost = 2;
            if (touchX >= 3 * minWidth) Controller.Player.ApCost = 3;

            _energyTagText.text = Controller.Player.ApCost.ToString();
            
            // Set appropriate color to energy tag if its available to use or not
            // Ready to use
            if (Controller.Player.APC.CurrentAp >= Controller.Player.ApCost)
            {
                _energyTagText.color = SetAlphaColor(_energyTagText.color, 1f);
                _energyTagImage.color = SetAlphaColor(_energyTagImage.color, 1f);
                
            }
            // Not ready to use
            else
            {
                _energyTagText.color = SetAlphaColor(_energyTagText.color, .5f);
                _energyTagImage.color = SetAlphaColor(_energyTagImage.color, .5f);
            }
        }

        // Set alpha to provided color
        private Color SetAlphaColor(Color color, float value)
        {
            Color tempColor;
            tempColor = color;
            tempColor.a = value;
            return tempColor;
        }

        // Check how many energy is available and set appropriate sizes
        private float CalculateWidthOnEnergyBarSize(float minWidth, float touchX)
        {
            float width;
            width = minWidth * Controller.Player.APC.CurrentAp + (minWidth * Controller.Player.APC.CurrentFill);
            if (width > touchX) width = touchX;
            
            return width;
        }

//        private void Rotate(Ray ray, float rayLenght)
//        {
//            Vector3 pointToLook = ray.GetPoint(rayLenght);
//            Debug.DrawLine(ray.origin, pointToLook, Color.green);
//            _lookPoint = pointToLook;
//            transform.LookAt(pointToLook);
//        }
    }
}