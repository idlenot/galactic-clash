//------------------------------------------------------------------------------
// PURPOSE:
// This script Controls animation and fill amount of the life or energy segment.
//------------------------------------------------------------------------------

using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GameSystem.UI
{
    [AddComponentMenu("IdleNot/UI/Segment")]
    public class Segment : MonoBehaviour
    {
        [SerializeField] private bool _isActive;                                   // Is the segment currently active to drop value from

        public bool IsActive
        {
            get => _isActive;
            set => _isActive = value;
        }

        [Header("Links")]
        [SerializeField] private Image _fill;                                      // Reference to Fill Image of the Segment gameObject
        public Image Fill => _fill;
        
        [SerializeField] private Image[] _colorImage;                                // Reference to Background Image of the Segment gameObject
        public Image[] ColorImage => _colorImage;
        
        [Header("Opacity")]
        [SerializeField] [Range(0,1)] private float _activeAlpha;                  // Color of Active segment
        public float ActiveAlpha => _activeAlpha;
        [SerializeField] [Range(0,1)] private float _notActiveAlpha;               // Color of not Active segment
        public float NotActiveAlpha => _notActiveAlpha;
        
        public void Initialize(Color color)
        {
            // Set segment color
            foreach (Image image in _colorImage) 
            {
                image.color = color;
            }
            SetColorState();
        }
        
        // Set segment Active
        public void Activate()
        {
            _isActive = true;
            SetSize(1.0f);
            SetColorState();
        }

        public virtual void SetColorState()
        {
            foreach (Image image in _colorImage) 
            {
                Color newColor = image.color;
                newColor.a = _isActive ? _activeAlpha : _notActiveAlpha;
                image.color = newColor;
            }
        }
        
        public void SetSize(float value)
        {
            if (value <= 0f)
            {
                value = 0f;
                _isActive = false;
                SetColorState();
            }

            if (value >= 1f)
            {
                value = 1f;
                _isActive = true;
                SetColorState();
            }
            _fill.fillAmount = value;
        }
        
        // Change values when attributes in editor has changed
        private void OnValidate()
        {
            Initialize(_colorImage[0].color);
        }
    }
}