﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script controls behaviour of visual spawn lane which is shown when
// a player drags unit card/icon from menu bar into the scene  
//------------------------------------------------------------------------------

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace GameSystem.UI
{
    [AddComponentMenu("IdleNot/UI/Spawn Lane")]
    [RequireComponent(typeof(Animator))]
    public class SpawnLane : MonoBehaviour
    {
        [SerializeField] private float _delay = 0.3f;                // Lag between next arrow to be shown
        [SerializeField] private Animator[] _arrowAnim;              // List of arrow images on the lane

        private bool _isActive;                                      // Is lane activated and visible
        public bool IsActive => _isActive;

        private Animator _anim;                                      // <Animator> Controller connected to this gameobject

        private void Awake()
        {
            _anim = GetComponent<Animator>();
            
            foreach (var image in _arrowAnim)
            {
                var color = image.GetComponent<Image>().color;
                color.a = 0f;
            }
        }

        private void Start()
        {
            Activate(false);
        }

        // Activate and show Spawn Lane
        public void Activate(bool state)
        {
            _isActive = state;
            _anim.SetBool("IsActive", state);
            if(gameObject.activeSelf) StartCoroutine(ActivateArrows(state, state ? _delay : 0f));
        }

        IEnumerator ActivateArrows(bool state, float delay)
        {
            foreach (Animator anim in _arrowAnim)
            {
                anim.SetBool("IsActive", state);
                yield return new WaitForSeconds(delay);
            }
            if (state) yield break;
            yield return new WaitForSeconds(1.0f);
            gameObject.SetActive(false);
        }
    }
}

