﻿using UnityEngine;
using GameSystem.Components;

namespace GameSystem.PowerUps
{
    // ------------------------------------------------------------------------
    // 
    // PowerUp Effect: Add provided _amount  value to target health 
    //
    // ------------------------------------------------------------------------
    
    [CreateAssetMenu (menuName = "Power Up/Health Buff")]
    public class HealthBuff : PowerupEffect
    {
        [SerializeField] private int _amount;
        
        public override void Apply(GameObject target)
        {
            //target.GetComponent<Health>().Value += _amount;
        }
    }
}
