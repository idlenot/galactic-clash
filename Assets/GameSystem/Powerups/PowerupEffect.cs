﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem.PowerUps
{
    // ------------------------------------------------------------------------
    // 
    // Blueprint of : Power Up Effects 
    //
    // ------------------------------------------------------------------------
    
    public abstract class PowerupEffect : ScriptableObject
    {
        public abstract void Apply(GameObject target);
    }
}