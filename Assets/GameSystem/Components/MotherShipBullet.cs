﻿using System.Collections;
using System.Collections.Generic;
using GameSystem.CustomVariables.Enums;
using GameSystem.Interfaces;
using UnityEngine;

namespace GameSystem.Components
{
    public class MotherShipBullet : Asset
    {
        [SerializeField] private float _speed = .05f;
        private Player _player;
        private int _power;

        public override void Initialize(UnitController controller, float duration)
        {
            _player = controller.Player;
            _power = controller.Unit.AttackPower;
            Invoke(nameof(Discard), duration);
        }

        // Move bullet forward
        private void FixedUpdate()
        {
            transform.Translate(Vector3.right * _speed);
        }

        private void Discard()
        {
            Destroy(gameObject);
        }

        // If Bullet hit enemy unit give damage and destroy self
        private void OnTriggerEnter(Collider other)
        {
            UnitController otherUnit = other.GetComponent<UnitController>();
            if (otherUnit != null && otherUnit.Player != _player)
            {
                int damage = _power * _player.ApCost;
                otherUnit.TakeDamage(damage);
                Destroy(gameObject);
            }
        }
    }
}