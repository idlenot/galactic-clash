﻿using UnityEngine;
using GameSystem.DataSets;


namespace GameSystem.Components
{
    // ------------------------------------------------------------------------
    // 
    // Asset : Abstract class of component of gameobject that is spawned on
    // the scene to provide any effect to the invoking unit or its surroundings 
    //
    // ------------------------------------------------------------------------

    
    public abstract class Asset : MonoBehaviour
    {
        public abstract void Initialize(UnitController controller, float duration);
    }

}