﻿using UnityEngine;
using UnityEngine.UI;

namespace GameSystem.Components
{
    //------------------------------------------------------------------------------
    // 
    // This script controls health bar of the actor unit.
    // 
    //------------------------------------------------------------------------------

    [AddComponentMenu("GC/Component/Unit Health Bar")]
    public class UnitHealthBar : HealthManager
    {
        [SerializeField] private Image _healthBar;
        
        // Health value of the unit
        private int _currentValue;                    // Current value of unit health

        
        public override void SetPosition(UnitController controller)
        {
            // Set position of this health bar to unit position it belongs to 
            transform.position = controller.transform.position + Vector3.up;
        }

        public override void Initialize(UnitController controller)
        {
            _currentValue = controller.Unit.Health;
            SetColor(controller.Player.Color);
            _healthBar.fillAmount = 1.0f;
        }

        // Change to color of controlling player
        private void SetColor(Color color)
        {
            _healthBar.color = color;
        }

        public override void AddValue(UnitController controller, int value)
        {
            _currentValue += value;
            if (_currentValue <= 0)
            {
                controller.Discard();
            }
            SetSize(controller);
        }

        // Set size of the UI health bar
        private void SetSize(UnitController controller)
        {
            _healthBar.fillAmount = Mathf.Clamp01((float)_currentValue/controller.Unit.Health);
        }
    }
}