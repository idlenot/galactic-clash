﻿using System.Collections.Generic;
using GameSystem.UI;
using UnityEngine;
using UnityEngine.UI;

namespace GameSystem.Components
{
    //------------------------------------------------------------------------------
    // 
    // This script controls health bar of the actor unit.
    // 
    //------------------------------------------------------------------------------

    [AddComponentMenu("GC/Component/Unit Health Bar")]
    public class UnitHealthToHudConnector : HealthManager
    {
        // Health value of the unit
        [SerializeField] private HudPlayerInfo _hud;

        
        public override void SetPosition(UnitController controller)
        { 
            transform.position = controller.transform.position + Vector3.up;
        }

        public override void Initialize(UnitController controller)
        {
            HudPlayerInfo[] hud = FindObjectsOfType<HudPlayerInfo>();
            for (int i = 0; i < hud.Length; i++)
            {
                if (hud[i].Player == controller.Player)
                {
                    _hud = hud[i];
                    controller.Player.CurrentHealth = controller.Unit.Health;
                    _hud.Initialize(controller.Player);
                }
                else Debug.Log(controller.name + " cannot find HUD!");
            }
        }

        public override void AddValue(UnitController controller, int value)
        {
            _hud.UpdateHealth(controller, value);
        }
    }
}