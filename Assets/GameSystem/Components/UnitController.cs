﻿// ••••II••DDDD••••LL••••••EEEEEE••••••NN••••NN••OOOO••••TTTTTT•••• //
// ••••II••DD••DD••LL••••••EE••••••••••NNNN••NN••OO••OO••••TT•••••• //
// ••••II••DD••DD••LL••••••EEEE••••••••NN••NNNN••OO••OO••••TT•••••• //
// ••••II••DDDD••••LLLLLL••EEEEEE••••••NN••••NN••••OOOO••••TT•••••• //

using System.Collections.Generic;
using System.Linq;
using Game;
using UnityEngine;
using UnityEngine.AI;
using GameSystem.AI.States;
using GameSystem.UI;
using GameSystem.DataSets;
using GameSystem.DataSets.Skills;
using GameSystem.CustomVariables.Enums;

namespace GameSystem.Components
{
    // ------------------------------------------------------------------------
    // 
    // Object Component : This controller:
    //
    // - controls AI, Skills and status effects of this unit
    // - stores and manage health
    //
    // ------------------------------------------------------------------------
    
    [AddComponentMenu("GC/Components/Unit Controller")]
    [RequireComponent(typeof(NavMeshAgent))]
    public class UnitController : MonoBehaviour
    {   
        [Header("Unit type")]
        public Unit Unit;                                        // Unit Archetype 
        public Player Player;                                    // What player this unit belongs to
        // public Player Enemy;                                     // Enemy of this unit
        
        [Header("Health Bar (if that unit should have any)")]
        public GameObject HealthBar;                             // Health bar prefab to spawn when unit appear on the arena 
        private HealthManager _health;                               // Reference to <HealthBar>

        [Header("Attached weapon game object")]
        public GameObject Weapon;                                // Child object that contains particles effects to be turned on while attacking
        
        [Header("Default AI State")]
        public State CurrentState;
        public State RemainState;
        [HideInInspector] public float StateTimeElapsed;         // How long this unit stays in the current state. Some states may last certain amount of time.
        
        [Header("Unit skills : OnBirth, OnUse, OnDeath")]
        public GameObject GuiElementPrefab;                      // Prefab of skill activation button to spawn and connect to this unity
        [HideInInspector]public GuiElement GuiElement;           // Reference to <SkillUseButton> subscribed to this particular unit
       
        [HideInInspector] public List<float> SkillTimeElapsed = new List<float>();          // How long current skill is being used
        [HideInInspector] public OnUseSkill ActiveSkill;         // Skill ready to activate  
        
        /// <summary>
        /// Zapytać: Czy zrobić listę aktywnych skilli oraz czy stworzyć Customowy Variably trzymający listę skilli i stan
        /// </summary>
        
        public Transform Target;//[HideInInspector] public Transform Target;               // Target transform this unit move towards to 
        [HideInInspector] public NavMeshAgent NavMeshAgent;        
        
        private bool _aiActive;                                  // Is AI activated? Allows AI and skills to do the job
        
        

        void Awake () 
        {
            NavMeshAgent = GetComponent<NavMeshAgent> ();
        }

        private void Start()
        {
            Invoke(nameof(DelayedStart), .1f);
        }

        // Delayed Start is for test purposes
        // TODO: For test purposes, delete when possible to omit this solution
        private void DelayedStart()
        {
            if (!_aiActive) Initialize(Player);
        }

        public void Initialize(Player player)
        {
            // Set to whom unit belong to
            Player = player;
            
            // Rotate to correct side
            transform.rotation = Quaternion.Euler(0, 90, 0);//new Quaternion(0, 0, 0, 0);
            
            // Deactivate weapons
            if (Weapon != null) Weapon.SetActive(false);
            else Debug.LogWarning(gameObject.name + " has no weapon object attached");
            
            // Set up Health Bar
            SetUpHealthBar();
            
            // Set up skills
            SetUpSkills();

            // Use On Birth Skill
            OnBirthSkill();
            
            // Prepare Nav Mesh Agent
            NavMeshAgent.Warp(transform.position);
            NavMeshAgent.speed = Unit.Speed;
            NavMeshAgent.stoppingDistance = Unit.AttackRange;
            NavMeshAgent.enabled = _aiActive = true;;
            
            // Apply additional effects if are added to this unit
            ApplyEffects();
            
            // Spawn Controller marker if 
            if (GuiElementPrefab != null && GuiElementPrefab.GetComponent<ShootMarker>()) SetUpController();
        }

        private void Update()
        {
            if (!_aiActive || !GameManager.GameIsOn) return;
            
            // Set position to health bar attached
            if (_health != null) _health.SetPosition(this);
            
            // Act current state this controller is under
            CurrentState.UpdateState(this);
            
            // Use skill/s that this controller has
            OnUseSkill();
            
        }

        public void SetUpController()
        {
            GameObject newGuiBtn = Instantiate(GuiElementPrefab);
            GuiElement = newGuiBtn.GetComponent<GuiElement>();
            GuiElement.Initialize(this);
        }


        // ------------------------------------
        // AI is controlled via FSM
        // ------------------------------------
        
        #region Final State Machine
        
        // Change behaviour state when new one has been provided based on AI decisions
        public void TransitionToState(State nextState)
        {
            if (nextState != RemainState)
            {
                CurrentState = nextState;
                OnChangeState();
            }
        }

        // Compare time elapsed from start of the current scene with provided duration, return true when reached 
        public bool CheckIfCountDownElapsed(float duration)
        {
            StateTimeElapsed += Time.deltaTime;
            return (StateTimeElapsed >= duration);
        }

        // What happen when transition between states takes place
        private void OnChangeState()
        {
            // Reset time how long current the state took place 
            StateTimeElapsed = 0;
        }
        
        // Disable AI when this game object is disabled
        private void OnDisable()
        {
            _aiActive = false;
        }
        
        #if UNITY_EDITOR
                        
                // Draw gizmos in editor
                // Attack range & Detector Range
                private void OnDrawGizmos()
                {
                    if (CurrentState != null)
                    {
                        Gizmos.color = CurrentState.SceneGizmoColor;
                        Gizmos.DrawWireSphere(transform.position, Unit.AttackRange);
                        Gizmos.color = Color.yellow;
                        Gizmos.DrawWireSphere(transform.position, Unit.DetectRange);
                    }
                }
        #endif    
        
        #endregion
        
        
        
        // ------------------------------------
        // Unit Health management
        // ------------------------------------

        #region Health management

        // Create health bar
        private void SetUpHealthBar()
        {
            // TODO: Instantiate through Object Pool
            if (HealthBar != null)
            {
                GameObject newHealthBar = Instantiate(HealthBar);
                _health = newHealthBar.GetComponent<HealthManager>();
                _health.Initialize(this);
            }
        }

        // Take damage
        public void TakeDamage(int amount)
        {
            if (_health != null) _health.AddValue(this, -amount);
        }

        // Destroy this unit
        // TODO: discard to pool
        public void Discard()
        {
            // Activate on death skill
            OnDeathSkill();
            // Destroy GUI element controller if attached any
            if (GuiElement != null) Destroy(GuiElement.gameObject);
            // Destroy _health element if attached any
            if (_health != null) Destroy(_health.gameObject);
            // Destroy self
            Destroy(gameObject);
        }

        #endregion
        
       
        
        // ------------------------------------
        // Unit Skill management
        // ------------------------------------

        #region Skill management
        
        
        // Set up skills of this unit
        private void SetUpSkills()
        {
            // if the unit archetype doesn't have any skills, return
            if (Unit.Skills.Length == 0) return;
            
            // Spawn use skill button
            if (GuiElementPrefab == null) return;
                
            // Prepare SkillTimeElapsed list
            if (SkillTimeElapsed.Any()) SkillTimeElapsed.Clear();
            for (int i = 0; i < Unit.Skills.Length; i++) SkillTimeElapsed.Add(0);
            
            // Create Skill button
            // TODO: Instantiate through Object Pool
            if (GuiElementPrefab == null)
            {
                Debug.LogWarning(gameObject.name + " has skill but no skill activation button prefab provided!");
                return;
            } 
            SetUpController();
        }

        
        // Activate skills on Enter
        private void OnBirthSkill()
        {
            for (int i = 0; i < Unit.Skills.Length; i++) Unit.Skills[i].OnBirth(this);
        }
        
        
        // Activate Manual skills
        private void OnUseSkill()
        {
            if (GuiElement != null) 
                for (int i = 0; i < Unit.Skills.Length; i++) Unit.Skills[i].OnUse(this);
        }
        
        
        // Activate skills on Exit
        private void OnDeathSkill()
        {
            for (int i = 0; i < Unit.Skills.Length; i++) Unit.Skills[i].OnDeath(this);
        }
        
        
        // Compare time elapsed from start of the current skill with provided duration, return true when reached 
        public bool CheckIfSkillTimeElapsed(float duration, Skill skill)
        {
            SkillTimeElapsed [GetIndexOf(skill)] += Time.deltaTime;
            // If second check met condition return true
            return (SkillTimeElapsed [GetIndexOf(skill)] >= duration);
        }

        
        // Reset timer of the provided skill 
        public void ResetSkillTimer(Skill skill)
        {
            SkillTimeElapsed [GetIndexOf(skill)] = 0;
        }

        // Check the index of provided <Skill> in <Unit.Skills> array
        private int GetIndexOf(Skill skill)
        {
            return System.Array.IndexOf(Unit.Skills, skill);
        }

        #endregion
        

        
        // ------------------------------------
        // Unit Skill management
        // ------------------------------------

        #region Status effects

        private void ApplyEffects()
        {
            
        }

        #endregion
    }
}