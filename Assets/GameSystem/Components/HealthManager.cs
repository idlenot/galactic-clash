﻿using UnityEngine;
using UnityEngine.UI;

namespace GameSystem.Components
{
    //------------------------------------------------------------------------------
    // 
    // This script controls health bar of the actor unit.
    // 
    //------------------------------------------------------------------------------

    public abstract class HealthManager : MonoBehaviour
    {
        public abstract void SetPosition(UnitController controller);
        public abstract void Initialize(UnitController controller);
        public abstract void AddValue(UnitController controller, int value);
    }
}