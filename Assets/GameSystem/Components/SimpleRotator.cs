﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameSystem.Components
{
    
    public class SimpleRotator : MonoBehaviour
    {
        [SerializeField] private float _speed;
        
        // Update is called once per frame
        void FixedUpdate()
        {
            transform.Rotate(Vector3.forward * _speed * Time.deltaTime);
        }
    }

}
