
    // ------------------------------------------------------------------------
    // 
    // ReadMe : DataEngine
    //
    // ------------------------------------------------------------------------
    
    In this folder stores scripts that are used in the game. 
     
    Every scripts should be independent from other 
    and serve only one functionality.
    
    It should be easy manageable to transfer script components to another game. 