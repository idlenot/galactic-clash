﻿using System.Collections;
using System.Collections.Generic;
using GameSystem.CustomVariables.Enums;
using UnityEngine;

namespace GameSystem.DataSets
{
    [CreateAssetMenu(menuName = "Game Data/Energy Controller")]
    public class EnergyController : ScriptableObject
    {
        // Maximum amount of energy
        [SerializeField] private int _totalEnergy = 10;  
        // How much energy player has on start
        [Range(0, 10)] [SerializeField] private int _startingEnergy = 5;
        // How long in seconds energy segment needs to fill up
        [Range(0, 10)] [SerializeField] private float _baseFillUpTime = 3.0f;
        // Is energy fill speed boosted (Last minute of the game)
        [HideInInspector] public bool EnergyBoostIsActive;
        
        public float FillUpTime {
            get
            {
                if (EnergyBoostIsActive) return _baseFillUpTime / 2;
                return _baseFillUpTime;
            } 
        }
        
        // Subtract energy from available pool
        public bool Use(Player player, int cost)
        {
            if (player.APC.CurrentAp < cost) return false;
            player.APC.CurrentAp -= cost;
            return true;            
        }
        
        // 
        public float EnergyUp(Player player, float value)
        {
            if (player.APC.CurrentAp >= _totalEnergy) return 0;
            if (value >= 1.0f)
            {
                player.APC.CurrentAp++;
                value = 0;
            }
            return value;
        }
    }
}
