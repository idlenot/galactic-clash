﻿using UnityEngine;

namespace GameSystem.DataSets
{
    
    // ------------------------------------------------------------------------
    // 
    // Attribute : Defines property of unit like speed, damage, health etc.
    //
    // ------------------------------------------------------------------------
    
    
    [CreateAssetMenu(menuName = "Game Data/Attribute")]
    public class Attributes : ScriptableObject
    {
        public string Description;
        public Sprite Image;
    }
}

