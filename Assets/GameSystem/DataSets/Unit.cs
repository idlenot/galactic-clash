﻿using GameSystem.DataSets.Skills;
using UnityEngine;

namespace GameSystem.DataSets
{
    
    //------------------------------------------------------------------------------
    //
    // PURPOSE:
    // This scriptable objects is an archetype of unit. Stores basic data
    // about units in game
    //
    //------------------------------------------------------------------------------

    
    [CreateAssetMenu(fileName = "Unit", menuName = "Game Data/Unit")]
    public class Unit : ScriptableObject
    {
        [Header("Support Data")]
        
        // Icon Image that show on cards
        public Sprite Icon;
        // Model prefab to be spawned on the scene
        public GameObject Prefab;
        // What theme this unit belongs to
        public string Theme;
        // What kind this unit is
        public string Type;
        // Description of the unit
        public string Description;
        // What is the cost in energy to use this unit
        public int Cost;
        
        [Header("Base Stats")]
        
        // Health of the unit
        public int Health;
        // How fast unit moves
        public float Speed;
        // How fast unit rotates
        public float RotationSpeed;
        
        
        [Header("Detection stats")]
        
        // Range the actor can detect enemy units
        public int DetectRange;                                          
        // How long the actor detects enemy in range before head to mother ship
        public float DetectDuration;                                     
        
        
        [Header("Combat stats")]
        
        // Attack power the actor has on start and restores to
        public int AttackPower;                               
        // Attack Range the actor has on start and restores to
        public int AttackRange;
        // Time between consecutive shoot the actor has on start and restores to
        public float AttackDelay;

        [Header("Skills this unit has")]

        // List of attached skills
        public Skill[] Skills;
    }
}