﻿using GameSystem.Components;
using UnityEngine;

namespace GameSystem.DataSets.Skills
{
    // ------------------------------------------------------------------------
    // 
    // Skill : On Birth - Activates after unit shows up on the arena
    //
    // ------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "Game Data/Skills/On Birth")]
    public class OnBirthSkill : Skill
    {
        public override void OnBirth(UnitController controller)
        {
            Use(controller);
        }
        
        public override void OnUse(UnitController controller)
        {
            // Not used here
        }
        
        public override void OnDeath(UnitController controller)
        {
            // Not used here
        }
    }
}