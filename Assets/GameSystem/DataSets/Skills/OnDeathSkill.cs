﻿using GameSystem.Components;
using UnityEngine;

namespace GameSystem.DataSets.Skills
{
    // ------------------------------------------------------------------------
    // 
    // Skill : On Death - Activates when unit dies
    //
    // ------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "Game Data/Skills/On Death")]
    public class OnDeathSkill : Skill
    {
        public override void OnBirth(UnitController controller)
        {
            // Not used here
        }
        
        public override void OnUse(UnitController controller)
        {
            // Not used here
        }
        
        public override void OnDeath(UnitController controller)
        {
            Use(controller);
        }
    }
}