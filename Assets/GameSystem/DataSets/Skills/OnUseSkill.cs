﻿using GameSystem.Components;
using UnityEngine;

namespace GameSystem.DataSets.Skills
{
    // ------------------------------------------------------------------------
    // 
    // Skill : Enter - Starts when unit spawns to arena
    //
    // ------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "Game Data/Skills/On Use")]
    public class OnUseSkill : Skill
    {
        // How long it takes before the skill can be used
        [Header("Time to activation")]
        [Tooltip("If 0 skill will not activate automatically")]
        [SerializeField] [Range(0, 10)] public float TimeToReady;
        
        public override void OnBirth(UnitController controller)
        {
            controller.ActiveSkill = this;
        }
        
        public override void OnUse(UnitController controller)
        {
            if (TimeToReady > 0)
                if (controller.CheckIfSkillTimeElapsed(TimeToReady, this)) 
                    Activate(controller);
        }
        
        public override void OnDeath(UnitController controller)
        {
            // Not used here
        }

        // Activate skill to use and show skill button 
        private void Activate (UnitController controller)
        {
            // Show Skill button
            if (controller.GuiElement.IsSeen()) return;
            controller.GuiElement.Show();
            
            // Provide Activation action here...
        }
        
        // Use skill if is ready
        public override void Use(UnitController controller)
        {
            base.Use(controller);
            controller.ResetSkillTimer(this);
        }
    }
}