﻿using System.Collections.Generic;
using UnityEngine;
using GameSystem.Components;
using Game;

namespace GameSystem.DataSets.Skills
{
    // ------------------------------------------------------------------------
    // 
    // Blueprint of : Skill SO for AI
    //
    // ------------------------------------------------------------------------

    public abstract class Skill : ScriptableObject
    {
        // What attributes the skill affects
        [Header("What effect/s this skill apply")]
        [Tooltip("List of all status effect/s will be applied to friendly or enemy units if any")]
        [SerializeField] protected List<Attributes> StatusEffects;

        [Header("How strong are skill effect/s")]
        [Tooltip("The amount int value that modifies attribute (in %)")]
        // How much chosen statistics change
        [SerializeField] [Range(0, 100)] protected int Amount;
        
        [Header("Duration of the skill")]
        [Tooltip("How long this skill lasts")]
        // How long effect takes place
        [SerializeField] [Range(0, 10)] protected float Duration;
        
        [Header("Asset to spawn")]
        [Tooltip("GameObject Asset to spawn when skill is activated")]
        // Any gameobject prefab to spawn when activated
        [SerializeField] protected GameObject Asset;

        // Actual doing of the skill
        // Use skill when unit has spawned
        public abstract void OnBirth(UnitController controller);
        // Use skill on specific moment like pressing button when ready
        public abstract void OnUse(UnitController controller);
        // Use skill when unit has been destroyed and discarded
        public abstract void OnDeath(UnitController controller);

        // Spawn assets if there are any
        public virtual void Use(UnitController controller)
        {
            // Debug.Log(name + " skill used with asset: " + Asset);
            if (Asset == null) return;
            GameObject newAsset = Instantiate(Asset, controller.gameObject.transform.position, Quaternion.identity);
            newAsset.transform.parent = GameManager.Instance.GameArea.transform;
            newAsset.GetComponent<Asset>().Initialize(controller, Duration);
        }
    }
}