﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GameSystem.DataSets
{
    //------------------------------------------------------------------------------
    // 
    // This script contains values needed for action point management further
    // in the game.
    // 
    //------------------------------------------------------------------------------

    [CreateAssetMenu(menuName = "Game Data/Action Points Controller")]
    public class ActionPointsController : ScriptableObject
    {
        // Maximum amount of AP
        public int TotalAp = 10;  
        // Current amount of AP
        [HideInInspector] public int CurrentAp = 10;
        // How much AP player has on start
        [Range(0, 10)] public int StartingAp = 5;
        // How long in seconds AP segment needs to fill up
        [Range(0, 10)] public float BaseFillUpTime = 3.0f;
        // Current fill percentage 0 - 1f
        [HideInInspector] public float CurrentFill;
    }
}
