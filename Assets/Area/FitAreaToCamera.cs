﻿

using UnityEngine;
using UnityEngine.Assertions;
using GameSystem.Helpers;

namespace Area
{
    
    [AddComponentMenu("IdleNot/Area/Fit area to camera")]
    [RequireComponent(typeof(GameArea))]
    public class FitAreaToCamera : MonoBehaviour
    {
        private GameArea _area
        {
            get 
            {
                if (_area == null) return GetComponent<GameArea>();
                else return _area;
            }
        }
    
        private void Start()
        {
            print(gameObject.name);
            FitToManCamera();
        }

        private void FitToCamera(Camera cam)
        {
            _area.Size = new Vector2(cam.aspect * cam.orthographicSize * 2, cam.orthographicSize * 2);
            transform.position = cam.transform.position;
            transform.rotation = cam.transform.rotation;
        }

        private void FitToManCamera()
        {
            Assert.IsNotNull(Camera.main);
            FitToCamera(Camera.main);
        }

        private void OnValidate()
        {
            FitToManCamera();
        }
    }
}