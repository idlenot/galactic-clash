﻿//------------------------------------------------------------------------------
// PURPOSE:
// This script shakes camera, when mother ship is hit
//------------------------------------------------------------------------------

using System.Collections;
using GameSystem.CustomVariables.Enums;
using GameSystem.UI;
using UnityEngine;

namespace Game.Camera
{
    [AddComponentMenu("IdleNot/Camera/Camera Shake")]
    public class CameraShake : MonoBehaviour
    {
        [Range(0f,2f)] [SerializeField] private float _duration = .5f;
        [Range(0f,2f)] [SerializeField] private float _magnitude = 1f;
        
        private void Start()
        {
            HudPlayerInfo.SegmentDestroyed += Shake;
        }

        private void Shake(Player player)
        {
            StartCoroutine(ShakeIE(_duration, _magnitude));
        }

        public IEnumerator ShakeIE(float duration, float magnitude)
        {
            float power = magnitude;
            Vector3 originalPos = transform.position;
            float elapsed = 0.0f;
            while (elapsed < duration)
            {
                // smooth out power amount in time
                power -= elapsed * power;
                // set new temp shake size
                float x = Random.Range(-1f, 1f) * power;
                float y = Random.Range(-1f, 1f) * power;
                float z = Random.Range(-1f, 1f) * power;
                Vector3 shakePos = new Vector3(x, y, 0);
                // set new shaken position
                transform.position = shakePos + originalPos;
                // count time elapsed 
                elapsed += Time.deltaTime;
                yield return null;
            }
            transform.position = originalPos;
            yield return null;
        }

        private void OnDisable()
        {
            HudPlayerInfo.SegmentDestroyed -= Shake;
        }
    }
}